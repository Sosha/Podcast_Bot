#!/usr/bin/python3

import telebot
import os.path
import os

bot = telebot.TeleBot("327090921:AAFWlGqM6dfRA2U4IPulz9_YthDCuX4CylE")

#P_PATH = "/home/soshaw/public_html/dl.soshaw.net/html/Podcasts"
P_PATH = "/home/sosha/dl.soshaw.net/html/Podcasts"
DL_PATH = "https://dl.soshaw.net/Podcasts"
F_SIZE = 40000000

state = {}

@bot.message_handler(commands=['start'])
def hello(m):
    bot.reply_to(m, "Hello!")


@bot.message_handler(commands=['ls'])
def list_dirs(m):
    dirs = os.listdir(P_PATH)
    keyboard = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    for i in dirs:
        keyboard.add(telebot.types.KeyboardButton(i))
    bot.send_message(m.chat.id, "Directories:", reply_markup=keyboard)


@bot.message_handler(func=lambda m: True)
def list_dir(m):
    keyboard = telebot.types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    if m.text in os.listdir(P_PATH):
        for i in os.listdir(os.path.join(P_PATH, m.text)):
            keyboard.add(telebot.types.KeyboardButton(i))
            state[m.chat.id] = m.text
        bot.send_message(m.chat.id, "Poscasts:", reply_markup=keyboard)
    else:
        try:
            bot.send_audio(m.chat.id, open(os.path.join(P_PATH, state[m.chat.id], m.text), "rb")) if os.path.getsize(os.path.join(P_PATH, state[m.chat.id], m.text)) < F_SIZE else bot.send_message(m.chat.id, os.path.join(DL_PATH, state[m.chat.id], m.text))
        except: 
            bot.send_message(m.chat.id, "Error")

bot.polling()

